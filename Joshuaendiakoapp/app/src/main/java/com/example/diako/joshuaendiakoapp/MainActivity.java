package com.example.diako.joshuaendiakoapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.SeekBar;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    int vermenigvuldigingsgetal = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView Uitrekkingvalue =  (TextView)findViewById(R.id.textView7);
        SeekBar Massa = (SeekBar)findViewById(R.id.seekBar2);
        SeekBar Veerconstante = (SeekBar)findViewById(R.id.seekBar);
        SeekBar Valversnelling = (SeekBar)findViewById(R.id.seekBar3);
        final TextView Massavalue = (TextView)findViewById(R.id.textView5);
        final TextView Veerconstantevalue = (TextView)findViewById(R.id.textView4);
        final TextView Valversnellingvalue = (TextView)findViewById(R.id.textView6);

        Uitrekkingvalue.setText("Uitrekking:");
        Massa.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress * vermenigvuldigingsgetal;
                Massavalue.setText(""+progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Veerconstante.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progress = progress * vermenigvuldigingsgetal;
                Veerconstantevalue.setText(""+progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Valversnelling.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                double getal = ((double)progress/5.00);
                Valversnellingvalue.setText(""+ getal);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }
}
